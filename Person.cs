namespace comp5002_10011751_assessment02
{

    class Person
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }


        public Person(string name, int id, string userName)
        {
            Name = name;
            Id = id;
            UserName = userName;
        }
    }
}




