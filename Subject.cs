namespace comp5002_10011751_assessment02
{
    
    class Subjects
    
    //strings for CourseCode, Course name and teacher 
    {
        public string CourseCode {get; set;}
        public string CourseName {get; set;}
        public string Teacher {get; set;}
             
        

        //Strings teacher name and course to writeline
        public Subjects(string teacher, string courseCode, string courseName)
        {
            Teacher = teacher;
            CourseName = courseName;
            CourseCode = courseCode;
        }
    }
}  