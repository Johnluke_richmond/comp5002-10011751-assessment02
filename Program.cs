﻿using System;
using System.Collections.Generic;

namespace comp5002_10011751_assessment02
{

    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            
            //STUDENT LISTS TO DISSLAY STUDENT NAME 
            Console.Clear();
            List<Student> studentList = new List<Student>();
            studentList.Add(new Student("Josh Mackenzie", 10011613, "FGT11"));
            studentList.Add(new Student("William Snook", 10009622, "BB71"));
            studentList.Add(new Student("Nick Bunce", 10011594, "MS39"));
            studentList.Add(new Student("Johnluke Richmond", 10011751, "GC25"));
            List<Teacher> teacherList = new List<Teacher>();
            
            //TEACHER LIST TO DISPLAY TEACHER NAME 
            teacherList.Add(new Teacher("Stefan Stasiewicz", 5004, "ITIF"));
            teacherList.Add(new Teacher("Jeffrey Kranenburg", 5002, "ITPR"));
            teacherList.Add(new Teacher("Ray Scoot", 5001, "PFSL"));
            teacherList.Add(new Teacher("Murray Foote", 5008, "SFTP"));
                        

            //SUBJECT LIST TO DISPLAY CODE, TEACHER NAME AND COURSE NAME
            List<Subjects> subjectsList = new List<Subjects>();
            subjectsList.Add(new Subjects("Stefan Stasiewicz", "COMP5004", "IT Infrastructure"));
            subjectsList.Add(new Subjects("Jeffrey Kranenburg", "COMP5002", "Intro To Programming"));
            subjectsList.Add(new Subjects("Ray Scoot", "COMP5001", "Professional Skills"));
            subjectsList.Add(new Subjects("Murray Foote", "COMP5008", "Software Packages"));
            

            //TO DISPLAY TEACHER AND STUDENT INFORMATION
            foreach (var item in teacherList)
            {
            Console.WriteLine(Teacher.WhatAmITeaching(subjectsList, item.Name));
            }
            foreach (var item in studentList)
            { 
            Console.WriteLine(Student.ListAllMySubjects(subjectsList, item.Name));
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }       
    }
}