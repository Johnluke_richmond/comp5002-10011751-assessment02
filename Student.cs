using System.Collections.Generic;

namespace comp5002_10011751_assessment02
{
    


    // TO LINK STUDENT NAME ON RIGHT LINE 
    class Student : Person
    {
        public Student(string name, int id, string userName) : base (name,  id, userName)
        {
        }
        
        //INTRODUCE SUBJECT # FOR STUDENT 
        public static string ListAllMySubjects(List<Subjects> subjectsList, string name ) 
        {
            var output = $"{name} is studying:\n";
            foreach (var x in subjectsList)
            {
                output += $"{x.CourseCode} - {x.CourseName}\n";       
            }
            return output;         
        } 
    }
}