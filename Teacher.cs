using System.Collections.Generic;

namespace comp5002_10011751_assessment02
{

    class Teacher : Person
    {
        public Teacher(string name, int id, string userName) : base (name,  id, userName)
        {
        }
        public static string WhatAmITeaching(List<Subjects> subjectsList, string name ) 
        {
            var output = "";
            for (var i = 0; i < subjectsList.Count; i++)
            {
                var lst = subjectsList [i];
                if (lst.Teacher == name)
                {
                output += $"{lst.Teacher} teaches:\n   {lst.CourseCode} - {lst.CourseName}\n";   
                } 
            }
            return output;         
        }            
    }
}



